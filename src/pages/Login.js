//imports
import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'

import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function Login(){

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {

		if(email !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [email, password])

	function loginUser(e){
		
		e.preventDefault()

		fetch('https://secret-bastion-87897.herokuapp.com/users/login', {

			method: 'POST',
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				email: email,
				password: password

			})

		})
		.then(res => res.json())
		.then( data => {

			if(data.accessToken){

				Swal.fire({

					icon: "success",
					title:"Login Successful!",
					text: `Thank you for logging in.`

				})

				localStorage.setItem('accessToken', data.accessToken)

				fetch('https://secret-bastion-87897.herokuapp.com/users/getUserDetails', {

					headers: {
						'Authorization':`Bearer ${localStorage.getItem('accessToken')}`
					}
					})
					.then(res => res.json())
					.then(data => {

					setUser({

						id: data._id,
						isAdmin: data.isAdmin

					})
				})


			}else{

				Swal.fire({

					icon: "error",
					title:"Login Failed!",
					text: data.message

				})

			}


		})


	}

	return (

		user.id
		?
		<Redirect to="/products" />
		:
		<>
			<h1 className = "text-center my-5" id="login-title">Login</h1>
			<Form onSubmit = {e => loginUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" value={email} onChange={e => {setEmail(e.target.value)}} placeholder="Enter Valid Email here" required />
				</Form.Group>
				<Form.Group controlId="userPassword">
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}} placeholder="Enter Password here" required />
				</Form.Group>
				{
					isActive
					?
					<Button variant="primary" type="submit" className="my-3">Submit</Button>
					:
					<Button variant="primary" className="my-3" disabled>Submit</Button>
				}
			</Form>
		</>

	)

}