import React, {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Form, Button} from 'react-bootstrap'
import UnauthorizedUser from './UnauthorizedUser'
import Swal from 'sweetalert2'
import {useHistory, Link} from 'react-router-dom'

export default function AddProduct(){

	const {user} = useContext(UserContext)

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [isActive, setIsActive] = useState(false)

	const history = useHistory()

	useEffect(() => {

		if(name !== "" && description !== "" && price !== "" ){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [name, description, price])

	function createProduct(e){

		e.preventDefault()

		fetch('https://secret-bastion-87897.herokuapp.com/products/addProduct',{

			method: 'POST',
			headers: {
				'Content-Type':'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}` 
			}, 
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then( data => {

			console.log(data)

			if(data.message !== "New Product Added"){

				Swal.fire({

					icon:"error",
					title:"Failed to Add Product.",
					text: data.message

				})
				

			}else{

				Swal.fire({

					icon:"success",
					title:"Product Successfully Added!",
					text: `You've added  ${data.name}`

				})

				history.push("/products")

			}

		})

		setName("")
		setDescription("")
		setPrice("")
			
	}
	return(

		user.isAdmin === false 
		?
		<UnauthorizedUser/>
		:
		<>
			<h1 className="my-5 text-center" id="addProd-title">Add Product</h1>
			<Form onSubmit={e => createProduct(e)}>
				<Form.Group>
					<Form.Label>Product Name:</Form.Label>
					<Form.Control type="text" value={name} onChange={e=>{setName(e.target.value)}} placeholder="Enter Product Name" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" value={description} onChange={e=>{setDescription(e.target.value)}} placeholder="Enter Description" required/> 
				</Form.Group>
				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control type="number" value={price} onChange={e=>{setPrice(e.target.value)}} placeholder="Enter price" required/> 
				</Form.Group>
				{
					isActive
					?
					<>
						<Button variant="primary" className="my-3" type="submit">Submit</Button>
						<Button variant="primary" className="my-3 mx-3" as={Link} to={"/products"} >Back</Button>
						
					</>
					:
					<>
						<Button variant="primary" className="my-3" disabled>Submit</Button>
						<Button variant="primary" className="my-3 mx-3" as={Link} to={"/products"}>Back</Button>
					</>
				}
			</Form>
		</>

	)

}