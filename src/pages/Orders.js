import React, {useState, useEffect, useContext} from 'react'
import {Link} from 'react-router-dom'
import {Card, ListGroup} from 'react-bootstrap'
import UserContext from '../UserContext'



export default function Order(){

	const {user} = useContext(UserContext)
	const [allOrders, setAllOrders] = useState([])

	useEffect(() =>{

		fetch('https://secret-bastion-87897.herokuapp.com/orders/getOrders', {

			headers:{
					'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}

		})
		.then(res => res.json())
		.then(data => {


			setAllOrders(data.map(order =>{
		
			return(
				
					<Card key={order._id} className="my-2">
						<Card.Header id="card-header">
							<Card.Title>Orders Purchased On: {order.datePurchased} </Card.Title>
						</Card.Header>
						<Card.Body>
							<Card.Text>Items:</Card.Text>
								{
									order.products.map(items=>{

									return (
										<ListGroup variant="flush" as="ul" key={items._id}>	
											<ListGroup.Item as="li">{items.name} - Quantity: {items.quantity}</ListGroup.Item>				
										</ListGroup>
									)

								})
								}
							<Card.Text className="my-1">Total:<span className="text-danger"> &#8369; {order.total}</span></Card.Text>		
						</Card.Body>
					</Card>

				)

			}))
		})
	},[])

	useEffect(() => {

		if(user.isAdmin){

			fetch('https://secret-bastion-87897.herokuapp.com/orders/getAllOrders', {

				headers:{
					'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
				}
			})
			.then(res => res.json())
			.then( data => {

				setAllOrders(data.map((order) =>{

					return(

						<Card key={order._id} className="my-2">
							<Card.Header id="card-header">
								<Card.Title >Orders for user {order.userId}</Card.Title>
							</Card.Header>
							<Card.Body>
								<Card.Text>Purchased On: {order.datePurchased}</Card.Text>
								<Card.Text>Items:</Card.Text>
									{
										order.products.map(items=>{

										return (
											<ListGroup variant="flush" as="ul" key={items._id}>	
												<ListGroup.Item as="li">{items.name} - Quantity: {items.quantity}</ListGroup.Item>				
											</ListGroup>
										)

									})
									}
								<Card.Text className="my-1">Total:<span className="text-danger"> &#8369; {order.total}</span></Card.Text>		
							</Card.Body>
						</Card>
						
					)
						
				}))

			})


		}

	}, [user])


	return(

			user.isAdmin
			?
			<>
				<h1 className ="text-center mt-5 order-header" >User Orders</h1>
				<div className="d-flex justify-content-center my-3">
					<Link className="mx-3 btn" id="btn-addProd" to={"/products"}>Dashboard</Link>
				</div>
				{allOrders}
			</>
			:
			<>
				<h1 className="my-5 text-center order-header" >Order History</h1>
				{allOrders}
			</>
		)
}