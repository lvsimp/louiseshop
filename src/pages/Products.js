import React, {useState, useEffect, useContext}from 'react'
import Product from '../components/Product'
import UserContext from '../UserContext'
import {Table, Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'



export default function Products() {

	const {user} = useContext(UserContext)
	

	const [productArray, setProductArray] = useState([])
	const [allProducts, setAllProduct] = useState([])
	const [update, setUpdate]= useState(0)

	

	//retrieving all active products for all users
	useEffect(() =>{

		fetch('https://secret-bastion-87897.herokuapp.com/products/activeProducts')
		.then(res => res.json())
		.then(data => {

			setProductArray(data.map(product =>{
		
			return(
				
				<Col xs={12} md={4} className="mt-4" key={product._id}>
					<Product key={product._id} productProps={product}/>
				</Col>

				)

			}))
		})
	},[])



	useEffect(() => {

		if(user.isAdmin){

			fetch('https://secret-bastion-87897.herokuapp.com/products/allProducts', {

				headers:{
					'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
				}
			})
			.then(res => res.json())
			.then( data => {

				setAllProduct(data.map((product) =>{

					return(

						<tr key={product._id}>
							<td>{product._id}</td>
							<td>{product.name}</td>
							<td>{product.price}</td>
							<td>{product.isActive ? "Enable" : "Disable"}</td>
							<td>
								{

									product.isActive
									?
									<>
										<Button variant="primary" className="mx-2" key={product._id} as={Link} to={`/updateProduct/${product._id}`}>Update</Button>
										<Button variant="danger" className="mx-2" onClick={()=>{archive(product._id)}}>Disabled</Button>
									</>
									:
									<>
										<Button variant="primary" className="mx-2" key={product._id} as={Link} to={`/updateProduct/${product._id}`}>Update</Button>
										<Button variant="success" className="mx-2" onClick ={() => {activate(product._id)}}>Enable</Button>
									</>
								}
							</td>
						</tr>
					)

				}))

			})


		}

	}, [user, update])



	function activate(productId){

		
		fetch(`https://secret-bastion-87897.herokuapp.com/products/activate/${productId}`, {

			method: 'PUT',
			headers:{
				Authorization : `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then( data => {

			console.log(data)
			setUpdate({})

		})

	}

	function archive(productId){


		fetch(`https://secret-bastion-87897.herokuapp.com/products/archive/${productId}`, {

			method: 'PUT',
			headers:{
				Authorization : `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then( data => {

			console.log(data)
			setUpdate({})

		})
			
	}


	return(

			user.isAdmin
			?
			<>
				<h1 className ="text-center mt-5" id="admin-dashboard">Admin Dashboard</h1>
				<div className=" d-flex justify-content-center my-3">
					<Link className="mx-3 btn" id="btn-addProd" to={"/addProduct"}>Add Product</Link>
					<Link className="mx-3 btn" id="btn-orders" to={"/order"}>Orders</Link>
				</div>
				<Table className="mt-3" striped bordered hover>
					<thead id = "table-head">
						<tr className="text-center">
							<th>ID</th>
							<th>Name</th>
							<th>Price</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{allProducts}
					</tbody>
				</Table>
			</>
			:
			<>
				<h1 className="my-5 text-center" id="availProd-title">Available Products</h1>
				<Row>
					{productArray}
				</Row>
			</>
		)
}