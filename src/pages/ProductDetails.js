import React, {useState, useEffect, useContext} from 'react'
import {Card, Button, Row, Col, InputGroup, FormControl} from 'react-bootstrap'
import {useParams, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'



export default function ProductDetails(){

	const {productId} = useParams()
	
	const {user} = useContext(UserContext)
	const [productDetails, setProductDetails] = useState({

		name: null,
		description: null, 
		price: null

	})

	useEffect(() => {

		fetch(`https://secret-bastion-87897.herokuapp.com/products/singleProducts/${productId}`)
		.then(res => res.json())
		.then(data => {

			if(data.message){

				Swal.fire({

					icon:"error",
					title: "Product Unavailable",
					text: data.message

				})

			}else{

				setProductDetails({

					name:data.name,
					description: data.description,
					price: data.price

				})
			}

		})

	}, [productId])

	return(

		<>
			<Row className="mt-5" key={productDetails._id}>
				<Col>
					<Card >
						<Card.Header className="text-center" id="product-header">{productDetails.name}</Card.Header>
						<Card.Body id="product-body" className="text-center">
						<Card.Text >{productDetails.description}</Card.Text>
						<Card.Text>Price:<span className="text-danger"> &#8369;{productDetails.price}</span></Card.Text>
						<Card.Text>Quantity:</Card.Text>
						<InputGroup className="mb-3">
						    <FormControl type="number"/ >
 						</InputGroup>
						{
							user.isAdmin === false
							?
							<Button variant="primary" block>Add To Cart</Button>
							:
							<Link className="btn btn-danger btn-block" to="/login">Login to Buy</Link>
						}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</>

	)
}