import React, {useState, useEffect} from 'react'

//import css
import './App.css';

//import components
import AppNavBar from './components/AppNavBar'

//import UserContext
import {UserProvider} from './UserContext'

//import pages
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Logout from './pages/Logout'
import Products from './pages/Products'
import ProductDetails from './pages/ProductDetails'
import AddProduct from './pages/AddProducts'
import UpdateProduct from './pages/UpdateProducts'
import Order from './pages/Orders'



//import router-dom
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'

import {Container} from 'react-bootstrap'

function App() {

  const [user, setUser] = useState({

    id: null,
    isAdmin: null

  })

  useEffect(() => {

    fetch('http://localhost:4000/users/getUserDetails', {

      headers: {
        'Authorization':`Bearer ${localStorage.getItem('accessToken')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      setUser({

        id: data._id,
        isAdmin: data.isAdmin

      })
    })

  }, [])
  
  //function to clear localStorage on logout    
  const unsetUser = () => {
    //clears content of our localStorage
    localStorage.clear()
  }


  return (

    <>
      <UserProvider value = {{user, setUser, unsetUser}}>
        <Router>
            <AppNavBar/>
            <Container>
              <Switch>
                <Route exact path="/" component={Home} />  
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/logout" component={Logout} />
                <Route exact path="/products" component={Products} />
                <Route exact path="/products/:productId" component={ProductDetails} />
                <Route exact path="/addProduct" component={AddProduct} />
                <Route exact path="/updateProduct/:productId" component={UpdateProduct} />
                <Route exact path="/order" component={Order} />
              </Switch> 
            </Container>
        </Router>
      </UserProvider>
    </>    
  );
}

export default App;
