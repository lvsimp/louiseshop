//imports
import React from 'react'
import {Row, Col, Jumbotron} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Banner({bannerProp}){


	return(

		<Row id="bannaer">
			<Col>
				<Jumbotron className="text-center mt-5 bg-transparent" >
					<h1 id="banner-title">{bannerProp.title}</h1>
					<p id="banner-desc" className="my-3">{bannerProp.description}</p>
					<Link to={bannerProp.destination} id="banner-btn" className="btn btn-info ">{bannerProp.buttonCallToAction}</Link>
				</Jumbotron>
			</Col>
		</Row>

	)

}