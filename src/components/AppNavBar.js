import React, {useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'

export default function AppNavbar(){

	const {user} = useContext(UserContext)	

	return(

		<Navbar expand="lg" id="nav-body" >
			<Navbar.Brand id="navbar-brand" as={Link} to="/">Louise Shop</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto" id="nav-link">
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					<Nav.Link as={Link} to="/products">Products</Nav.Link>
					{
						user.id
						?
							user.isAdmin
							?
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={Link} to="/cart">Cart</Nav.Link>
								<Nav.Link as={Link} to="/order">Order History</Nav.Link>
								<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
						:
						<>

							<Nav.Link as={Link} to="/login">Login</Nav.Link>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
							
						</>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>

	)

}