import React from 'react'
import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Product({productProps}){

	return(
		
		<Card key={productProps._id}>
			<Card.Body>
				<Card.Title as={Link} to={`/products/${productProps._id}`}><h4 className="text-center mb-4" id="card-title">{productProps.name}</h4></Card.Title>
				<Card.Text>{productProps.description}</Card.Text>
				<Card.Text className="text-danger">&#8369; {productProps.price}</Card.Text>
			</Card.Body>
			<Card.Footer>
				<Link className="btn btn-primary" to={`/products/${productProps._id}`}>Details</Link>
			</Card.Footer>
		</Card>
	)

}